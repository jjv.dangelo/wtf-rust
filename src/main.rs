// #![windows_subsystem = "windows"]

use cgmath::{prelude::*, *};
use std::{
    mem, thread,
    time::{Duration, Instant},
};
use windows::{
    core::Result,
    s, w,
    Win32::{
        Foundation::*,
        Graphics::{
            Direct3D::{Fxc::*, *},
            Direct3D11::*,
            Dxgi::{Common::*, *},
        },
        Media::{timeBeginPeriod, timeEndPeriod, TIMERR_NOERROR},
        System::LibraryLoader::*,
        UI::{Input::KeyboardAndMouse::*, WindowsAndMessaging::*},
    },
};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Vertex {
    pub pos: cgmath::Vector3<f32>,
    pub col: cgmath::Vector4<f32>,
}

impl Vertex {
    #[inline]
    pub fn new(pos: cgmath::Vector3<f32>, col: cgmath::Vector4<f32>) -> Self {
        Self { pos, col }
    }
}

pub struct Camera {
    pub eye: Point3<f32>,
    pub look_at: Point3<f32>,
    pub up: Vector3<f32>,
    pub aspect_ratio: f32,
    pub fov: f32,
    pub near: f32,
    pub far: f32,
}

impl Camera {
    pub fn new((screen_width, screen_height): (i32, i32)) -> Self {
        let aspect_ratio = screen_width as f32 / screen_height as f32;

        Self {
            eye: point3(0., 0., 5.),
            look_at: point3(0., 0., 0.),
            up: vec3(0., 1., 0.),
            aspect_ratio,
            fov: std::f32::consts::FRAC_PI_2,
            near: 0.01,
            far: 100.,
        }
    }

    pub fn get_transform(&self) -> Matrix4<f32> {
        let perspective_trans =
            cgmath::perspective(Rad(self.fov), self.aspect_ratio, self.near, self.far);
        let look_at_trans = cgmath::Matrix4::look_at_rh(self.eye, self.look_at, self.up);

        perspective_trans * look_at_trans
    }
}

pub struct Timer {
    current_time: Instant,
    target_frame_rate: Duration,
    total_time: Duration,
    total_ticks: u64,
    pub fps: f32,
    sleep_is_granular: bool,
}

const TIME_GRANULARITY: u32 = 10;

impl Timer {
    pub fn new() -> Self {
        let current_time = Instant::now();
        let target_frame_rate = Duration::from_secs_f32(1. / 30.);
        let sleep_is_granular = unsafe { timeBeginPeriod(TIME_GRANULARITY) == TIMERR_NOERROR };

        println!("granular: {}", sleep_is_granular);

        Self {
            current_time,
            target_frame_rate,
            total_time: Duration::ZERO,
            total_ticks: 0,
            fps: 0.,
            sleep_is_granular,
        }
    }

    pub fn begin_frame(&mut self) -> Tick {
        Tick {
            dt: self.target_frame_rate,
        }
    }

    pub fn end_frame(&mut self) {
        let mut elapsed = self.current_time.elapsed();
        if elapsed < self.target_frame_rate {
            if self.sleep_is_granular {
                thread::sleep(self.target_frame_rate - elapsed);
            }

            elapsed = self.current_time.elapsed();
            while elapsed < self.target_frame_rate {
                elapsed = self.current_time.elapsed();
            }
        }

        self.total_time += self.current_time.elapsed();
        self.total_ticks += 1;
        self.fps = 1. / elapsed.as_secs_f32();
        self.current_time = Instant::now();
    }
}

impl Drop for Timer {
    fn drop(&mut self) {
        unsafe { timeEndPeriod(TIME_GRANULARITY) };
    }
}
unsafe extern "system" fn wnd_proc(
    hwnd: HWND,
    msg: u32,
    wparam: WPARAM,
    lparam: LPARAM,
) -> LRESULT {
    if msg == WM_DESTROY {
        PostQuitMessage(0);
        return LRESULT(0);
    }

    DefWindowProcA(hwnd, msg, wparam, lparam)
}

#[derive(Clone, Copy)]
pub struct Tick {
    dt: Duration,
}

fn main() -> Result<()> {
    fn window_dim(hwnd: HWND) -> (i32, i32) {
        let mut rect = RECT::default();
        unsafe { GetClientRect(hwnd, &mut rect) };

        (rect.right - rect.left, rect.bottom - rect.top)
    }

    let instance = unsafe { GetModuleHandleA(None) }?;

    let class_name = s!("Window Class");
    {
        let class = WNDCLASSA {
            style: CS_OWNDC,
            hCursor: unsafe { LoadCursorW(HINSTANCE(0), IDC_ARROW) }?,
            lpfnWndProc: Some(wnd_proc),
            lpszClassName: class_name,
            ..Default::default()
        };

        assert!(unsafe { RegisterClassA(&class) } != 0);
    }

    let hwnd = unsafe {
        CreateWindowExA(
            WINDOW_EX_STYLE(0),
            class_name,
            s!("Window Name"),
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            HWND(0),
            HMENU(0),
            instance,
            None,
        )
    };

    assert!(hwnd != HWND(0), "Invalid window handle");

    let (device, device_context) = unsafe {
        let mut device = None;
        let mut device_context = None;

        let mut feature_level = D3D_FEATURE_LEVEL_11_0;

        let flags = D3D11_CREATE_DEVICE_SINGLETHREADED
            | D3D11_CREATE_DEVICE_FLAG(D3D11_CREATE_DEVICE_DEBUG.0 * cfg!(debug_assertions) as u32);

        D3D11CreateDevice(
            None,
            D3D_DRIVER_TYPE_HARDWARE,
            None,
            flags,
            None,
            D3D11_SDK_VERSION,
            Some(&mut device),
            Some(&mut feature_level),
            Some(&mut device_context),
        )?;

        (device.unwrap(), device_context.unwrap())
    };

    let factory = unsafe { CreateDXGIFactory1::<IDXGIFactory2>() }?;

    let swapchain: IDXGISwapChain1 = unsafe {
        let swap_chain_desc = DXGI_SWAP_CHAIN_DESC1 {
            Width: 0,
            Height: 0,
            Format: DXGI_FORMAT_B8G8R8A8_UNORM,
            SampleDesc: DXGI_SAMPLE_DESC {
                Count: 1,
                Quality: 0,
            },
            BufferUsage: DXGI_USAGE_RENDER_TARGET_OUTPUT,
            BufferCount: 2,
            SwapEffect: DXGI_SWAP_EFFECT_FLIP_DISCARD,
            Scaling: DXGI_SCALING_NONE,
            ..Default::default()
        };

        let swap_chain_fs_desc = DXGI_SWAP_CHAIN_FULLSCREEN_DESC {
            Windowed: TRUE,
            ..Default::default()
        };

        factory.CreateSwapChainForHwnd(
            &device,
            hwnd,
            &swap_chain_desc,
            Some(&swap_chain_fs_desc),
            None,
        )
    }?;

    let render_target = {
        let back_buffer: ID3D11Texture2D = unsafe { swapchain.GetBuffer(0) }?;
        let mut render_target: Option<ID3D11RenderTargetView> = None;
        unsafe { device.CreateRenderTargetView(&back_buffer, None, Some(&mut render_target)) }?;
        render_target.unwrap()
    };

    let shader_file = w!("shaders.hlsl");
    let flags = D3DCOMPILE_ENABLE_STRICTNESS | (D3DCOMPILE_DEBUG * cfg!(debug_assertions) as u32);
    let mut vs_blob = None;
    unsafe {
        D3DCompileFromFile(
            shader_file,
            None,
            None,
            s!("vs_main"),
            s!("vs_5_0"),
            flags,
            0,
            &mut vs_blob,
            None,
        )
    }?;
    let vs_blob = vs_blob.unwrap();

    let mut ps_blob = None;
    unsafe {
        D3DCompileFromFile(
            shader_file,
            None,
            None,
            s!("ps_main"),
            s!("ps_5_0"),
            flags,
            0,
            &mut ps_blob,
            None,
        )
    }?;
    let ps_blob = ps_blob.unwrap();

    let mut vertex_shader = None;
    let vs_blob = unsafe {
        std::slice::from_raw_parts(
            vs_blob.GetBufferPointer() as *const u8,
            vs_blob.GetBufferSize() as usize,
        )
    };
    unsafe { device.CreateVertexShader(vs_blob, None, Some(&mut vertex_shader)) }?;
    let vertex_shader = vertex_shader.unwrap();

    let mut pixel_shader = None;
    let ps_blob = unsafe {
        std::slice::from_raw_parts(
            ps_blob.GetBufferPointer() as *const u8,
            ps_blob.GetBufferSize() as usize,
        )
    };
    unsafe { device.CreatePixelShader(ps_blob, None, Some(&mut pixel_shader)) }?;
    let pixel_shader = pixel_shader.unwrap();

    let input_element_desc = [
        D3D11_INPUT_ELEMENT_DESC {
            SemanticName: s!("POS"),
            SemanticIndex: 0,
            Format: DXGI_FORMAT_R32G32B32_FLOAT,
            InputSlot: 0,
            AlignedByteOffset: 0,
            InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
            InstanceDataStepRate: 0,
        },
        D3D11_INPUT_ELEMENT_DESC {
            SemanticName: s!("COL"),
            SemanticIndex: 0,
            Format: DXGI_FORMAT_R32G32B32A32_FLOAT,
            InputSlot: 0,
            AlignedByteOffset: D3D11_APPEND_ALIGNED_ELEMENT,
            InputSlotClass: D3D11_INPUT_PER_VERTEX_DATA,
            InstanceDataStepRate: 0,
        },
    ];

    let mut input_layout = None;
    unsafe { device.CreateInputLayout(&input_element_desc, vs_blob, Some(&mut input_layout)) }?;
    let input_layout = input_layout.unwrap();

    const MAX_QUADS: usize = 4096;
    let vertex_buffer = {
        let vertex_buffer_desc = D3D11_BUFFER_DESC {
            ByteWidth: (MAX_QUADS * 4 * mem::size_of::<Vertex>()) as _,
            Usage: D3D11_USAGE_DYNAMIC,
            CPUAccessFlags: D3D11_CPU_ACCESS_WRITE,
            BindFlags: D3D11_BIND_VERTEX_BUFFER,
            ..Default::default()
        };

        let mut vertex_buffer = None;
        unsafe { device.CreateBuffer(&vertex_buffer_desc, None, Some(&mut vertex_buffer)) }?;

        vertex_buffer
    };

    let _index_buffer = {
        let index_buffer_desc = D3D11_BUFFER_DESC {
            ByteWidth: (mem::size_of::<u32>() * 6 * MAX_QUADS) as _,
            Usage: D3D11_USAGE_DEFAULT,
            BindFlags: D3D11_BIND_INDEX_BUFFER,
            ..Default::default()
        };

        let index_buffer = {
            let mut buffer = vec![0u32; MAX_QUADS * 6];

            for quad in 0..MAX_QUADS {
                buffer[quad * 6 + 0] = (quad * 4 + 0) as u32;
                buffer[quad * 6 + 1] = (quad * 4 + 1) as u32;
                buffer[quad * 6 + 2] = (quad * 4 + 3) as u32;
                buffer[quad * 6 + 3] = (quad * 4 + 3) as u32;
                buffer[quad * 6 + 4] = (quad * 4 + 1) as u32;
                buffer[quad * 6 + 5] = (quad * 4 + 2) as u32;
            }

            buffer
        };

        let sr_data = D3D11_SUBRESOURCE_DATA {
            pSysMem: index_buffer.as_ptr() as _,
            ..Default::default()
        };

        let mut index_buffer = None;
        unsafe {
            device.CreateBuffer(&index_buffer_desc, Some(&sr_data), Some(&mut index_buffer))
        }?;

        let index_buffer = index_buffer.unwrap();

        unsafe { device_context.IASetIndexBuffer(&index_buffer, DXGI_FORMAT_R32_UINT, 0) };

        index_buffer
    };

    let constant_buffer = {
        let constant_buffer_desc = D3D11_BUFFER_DESC {
            ByteWidth: mem::size_of::<cgmath::Matrix4<f32>> as _,
            Usage: D3D11_USAGE_DYNAMIC,
            BindFlags: D3D11_BIND_CONSTANT_BUFFER,
            CPUAccessFlags: D3D11_CPU_ACCESS_WRITE,
            ..Default::default()
        };

        let mut constant_buffer = None;
        unsafe { device.CreateBuffer(&constant_buffer_desc, None, Some(&mut constant_buffer)) }?;

        constant_buffer.unwrap()
    };

    unsafe {
        device_context.VSSetConstantBuffers(0, Some(&[constant_buffer.clone()]));
    }

    unsafe { ShowWindow(hwnd, SHOW_WINDOW_CMD(SHOW_OPENWINDOW)) };

    let mut timer = Timer::new();
    let mut camera = Camera::new(window_dim(hwnd));

    let mut up = 0.;
    let mut down = 0.;
    let mut left = 0.;
    let mut right = 0.;

    let mut msg = MSG::default();
    'main: loop {
        unsafe {
            while PeekMessageA(&mut msg, HWND(0), 0, 0, PM_REMOVE).as_bool() {
                TranslateMessage(&mut msg);
                DispatchMessageA(&mut msg);

                match msg.message {
                    WM_KEYDOWN => match VIRTUAL_KEY(msg.wParam.0 as _) {
                        VK_W => up = -1.,
                        VK_S => down = 1.,
                        VK_A => left = 1.,
                        VK_D => right = -1.,
                        _ => (),
                    },

                    WM_KEYUP => match VIRTUAL_KEY(msg.wParam.0 as _) {
                        VK_W => up = 0.,
                        VK_S => down = 0.,
                        VK_A => left = 0.,
                        VK_D => right = 0.,
                        VK_ESCAPE => break 'main,
                        _ => (),
                    },

                    WM_QUIT => break 'main,

                    _ => (),
                }
            }

            let tick = timer.begin_frame();
            let dt = tick.dt.as_secs_f32();

            let movement = vec3(left + right, up + down, 0.) * dt;
            camera.eye += movement;
            camera.look_at += movement;

            timer.end_frame();
            println!("FPS: {}", timer.fps);

            let (window_width, window_height) = window_dim(hwnd);
            let viewport = D3D11_VIEWPORT {
                TopLeftX: 0.,
                TopLeftY: 0.,
                Width: window_width as f32,
                Height: window_height as f32,
                MinDepth: 0.,
                MaxDepth: 1.,
            };

            let clear_color = vec4(0.1, 0.6, 0.8, 1.);
            device_context.ClearRenderTargetView(&render_target, clear_color.as_ptr());
            device_context.RSSetViewports(Some(&[viewport]));

            {
                let mut mapped_resource = D3D11_MAPPED_SUBRESOURCE::default();
                device_context.Map(
                    &constant_buffer.clone(),
                    0,
                    D3D11_MAP_WRITE_DISCARD,
                    0,
                    Some(&mut mapped_resource),
                )?;

                let ptr = mapped_resource.pData as *mut cgmath::Matrix4<f32>;
                ptr.write(camera.get_transform());

                device_context.Unmap(&constant_buffer.clone(), 0);
            }

            let mut vertices = Vec::new();
            let mut num_quads = 0;

            for x in -5..6 {
                let color = vec4(0.25, 0.35, 0.6, 1.);
                let pos = vec3(x as _, 0., 0.);

                let bottom_left = pos + vec3(-0.5, -0.5, 0.);
                let bottom_right = pos + vec3(0.5, -0.5, 0.);
                let top_right = pos + vec3(0.5, 0.5, 0.);
                let top_left = pos + vec3(-0.5, 0.5, 0.);

                vertices.push(Vertex::new(bottom_left, color));
                vertices.push(Vertex::new(top_left, color));
                vertices.push(Vertex::new(top_right, color));
                vertices.push(Vertex::new(bottom_right, color));

                num_quads += 1;
            }

            {
                let mut mapped_resource = D3D11_MAPPED_SUBRESOURCE::default();
                device_context.Map(
                    &vertex_buffer.clone().unwrap(),
                    0,
                    D3D11_MAP_WRITE_DISCARD,
                    0,
                    Some(&mut mapped_resource),
                )?;

                let dest = mapped_resource.pData as *mut Vertex;
                let num_verts = vertices.len();
                std::ptr::copy_nonoverlapping(vertices.as_ptr(), dest, num_verts as _);

                device_context.Unmap(&vertex_buffer.clone().unwrap(), 0);
            }

            let vertex_stride = mem::size_of::<Vertex>() as u32;
            let vertex_offset = 0;
            let index_count = num_quads * 6;

            device_context.IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
            device_context.IASetInputLayout(&input_layout);
            device_context.IASetVertexBuffers(
                0,
                1,
                Some(&vertex_buffer),
                Some(&vertex_stride),
                Some(&vertex_offset),
            );

            device_context.VSSetShader(&vertex_shader, None);
            device_context.PSSetShader(&pixel_shader, None);
            device_context.OMSetRenderTargets(Some(&[render_target.clone()]), None);
            device_context.DrawIndexed(index_count as _, 0, 0);

            let _ = swapchain.Present(1, 0);
        }
    }

    Ok(())
}
